# frozen_string_literal: true

# Manages recipes
class RecipesController < ApplicationController
  def index
    action = FetchRecipes.call
    render render_action_response(action)
  end

  def show
    action = FetchRecipe.call(permitted_params)
    render render_action_response(action)
  end

  private

  def permitted_params
    params.permit(:id)
  end

  # Utility method to create consistent responses from interactor action objects
  def render_action_response(action)
    return { json: { data: action.response }, status: :ok } if action.success?

    { json: { message: action.message }, status: :bad_request }
  end
end
