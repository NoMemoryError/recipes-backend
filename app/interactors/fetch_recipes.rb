# frozen_string_literal: true

# Fetches recipes collection from content_delivery_api
class FetchRecipes
  include Interactor
  include ContentDeliveryApiClient

  def call
    entries = client.entries(content_type: 'recipe')
    context.response = entries.map do |entry|
      {
        id: entry.id,
        title: entry.title,
        tags: entry.fields.key?(:tags) ? entry.tags.map(&:name) : [],
        description: entry.description
      }
    end
  end
end
