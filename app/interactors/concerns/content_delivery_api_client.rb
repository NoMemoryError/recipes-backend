# frozen_string_literal: true

# Provides content_delivery_api client related methods for reusability purpose
module ContentDeliveryApiClient
  private

  def client
    # TODO: space_id and access_token needs to be refactored in credentials file
    @client ||=
      Contentful::Client.new(space: 'kk2bw5ojx476',
                             access_token: '7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c',
                             dynamic_entries: :auto)
  end
end
