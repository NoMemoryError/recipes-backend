# frozen_string_literal: true

# Fetch individual recipe from content_delivery_api
class FetchRecipe
  include Interactor
  include ContentDeliveryApiClient

  delegate :id, to: :context

  def call
    context.fail!(message: 'Recipe id is mandatory') if id.nil?
    entry = client.entry id
    context.fail!(message: 'Invalid recipe id') if entry.nil?
    response(entry)
  end

  private

  def response(entry)
    context.response = {
      id: entry.id,
      title: entry.title,
      tags: entry.fields.key?(:tags) ? entry.tags.map(&:name) : [],
      description: entry.description,
      image_url: image_url(entry),
      chef_name: entry.fields.key?(:chef) ? entry.chef.name : nil
    }
  end

  def image_url(entry)
    return unless entry.fields.key?(:photo)

    image_url = entry.photo.url
    image_url.start_with?('http') ? image_url : "https:#{image_url}"
  end
end
