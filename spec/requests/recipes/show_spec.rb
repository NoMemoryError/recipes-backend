# frozen_string_literal: true

RSpec.describe 'GET /recipes/:id' do
  let(:data) { { id: 'id_1', title: 'title_1', description: 'description_1' } }

  let(:context) { double(:context, success?: true, response: data) }

  before { allow(FetchRecipe).to receive(:call).and_return(context) }

  it 'returns with success' do
    get '/recipes/valid_id'
    expect(response.status).to eq(200)
    expect(response.body).to include_json(data: data)
  end
end
