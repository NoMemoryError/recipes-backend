# frozen_string_literal: true

RSpec.describe 'GET /recipes' do
  let(:data) do
    [
      { id: 'id_1', title: 'title_1', description: 'description_1' },
      { id: 'id_2', title: 'title_2', description: 'description_2' }
    ]
  end
  let(:context) { double(:context, success?: true, response: data) }

  before { allow(FetchRecipes).to receive(:call).and_return(context) }

  it 'returns with success' do
    get '/recipes'
    expect(response.status).to eq(200)
    expect(response.body).to include_json(data: data)
  end
end
