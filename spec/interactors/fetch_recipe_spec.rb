# frozen_string_literal: true

RSpec.describe FetchRecipe do
  subject { described_class.call(id: id) }

  context 'without any id' do
    let(:id) { nil }

    it { is_expected.to be_failure }
  end

  context 'with id' do
    context 'valid' do
      let(:id) { 'valid_id' }

      # HACK: Unfortunate workaround 🐞 since the contentful gem provides no helpers to mock the responses.
      Entry = Struct.new(:id, :title, :description, :fields, keyword_init: true) do
        def initialize(*)
          super
          self.fields ||= {}
        end
      end

      before do
        allow_any_instance_of(Contentful::Client).to receive(:entry)
          .with(id)
          .and_return(Entry.new(id: 'id1', title: 'title1', description: 'desc1'))
      end

      it { is_expected.to be_success }
    end

    context 'invalid' do
      let(:id) { 'not_found' }

      before do
        allow_any_instance_of(Contentful::Client).to receive(:entry)
          .with(id)
          .and_return(nil)
      end

      it { is_expected.to be_failure }
    end
  end
end
