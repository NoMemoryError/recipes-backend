# frozen_string_literal: true

RSpec.describe FetchRecipes do
  subject { described_class.call }

  # HACK: Unfortunate workaround 🐞 since the contentful gem provides no helpers to mock the responses.
  Entry = Struct.new(:id, :title, :description, :fields, keyword_init: true) do
    def initialize(*)
      super
      self.fields ||= {}
    end
  end

  before do
    allow_any_instance_of(Contentful::Client).to receive(:entries)
      .with(content_type: 'recipe')
      .and_return([
                    Entry.new(id: 'id1', title: 'title1', description: 'desc1'),
                    Entry.new(id: 'id2', title: 'title2', description: 'desc2')
                  ])
  end

  it { is_expected.to be_success }
end
