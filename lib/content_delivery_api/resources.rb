# frozen_string_literal: true

require_relative 'api_request'

module ContentDeliveryApi
  # Exposes methods to query various content_delivery_api endpoints
  class Resources
    class << self
      # TODO: Move keys in credentials file and log error when status_code is > 300
      def fetch_entries_collection
        full_path = api_path
        ApiRequest.execute(:get, full_path)
      end

      def fetch_entry(entry_id)
        full_path = api_path(entry_id)
        ApiRequest.execute(:get, full_path)
      end

      private

      def api_path(entry_id = nil)
        space_id = 'kk2bw5ojx476'
        access_token = '7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c'
        ['spaces', space_id, 'entries', entry_id].reject(&:nil?)
                                                 .join('/')
                                                 .concat("?access_token=#{access_token}")
      end
    end
  end
end
