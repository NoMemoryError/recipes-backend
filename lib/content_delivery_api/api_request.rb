# frozen_string_literal: true

module ContentDeliveryApi
  # Simplifies making API requests to ContentDeliveryApi
  module ApiRequest
    API_BASE_URL = 'https://cdn.contentful.com' # can be moved to settings for having it easily configurable

    class << self
      # Execute the API request and return both status and the response parsed as JSON
      def execute(verb, path, body = nil)
        path = "/#{path}" unless path.start_with?('/', 'http')
        body = body.to_json unless body.nil? || body.is_a?(String)
        response = http.send(verb, path, body: body)
        [response.status, Oj.load(response.body.to_s)]
      end

      private

      def http
        @http ||= HTTP.persistent(API_BASE_URL)
                      .headers(accept: 'application/json', content_type: 'application/json')
      end
    end
  end
end
